<!DOCTYPE html>
<html>
<head>
<style>
	h1 {
		text-align: center;
		}
</style>
	<meta charse="utf-8">
    <title></title>
</head>
<body>
	<a id="top"></a>
	<!--content here-->
    <h1>Assignment03-Chen,Jinghang</h1>
<!-- Section 1 -->
<p>
	<h3>Section 1:Typical Grocery List</h3>
	<ul>
		<li>Bacon</li>
	    <li>Napa Cabbage</li>
	    <li>Cauliflower</li>
		<li>Spainch</li>
		<li>Egg</li>
	</ul>
</p>
<p>
    <h3>Section 2:List of States and Cities</h3>
    <ul>
                <li>Ohio</li>
                 <ul>
                <li> Toledo </li>
                <li> Bowling Green </li>
                <li> Columbus </li>
				<li> Cincinatti</li>
                </ul>
               <li>Michigan</li>
               <ul>
                   <li> Detroit </li>
                   <li> Grand Rapids </li>
                   <li> Flint </li>
				   <li> Kalamazoo </li>
               </ul>
               <li>California</li>
               <ul>
                   <li> Los Angels </li>
                   <li> San Fransico </li>
                   <li> San Deigo </li>
				   <li> San Jose </li>
               </ul>
			   <li>Florida</li>
			   <ul>
				   <li> Miami </li>
				   <li> Tampa </li>
				   <li> Orlando </li>
				   <li> Key West </li>
			   </ul>
    </ul>
       </p>
<p>
	<h3>Section 3:Definition List</h3>
	<dl>
		<dt>Lord of rings</dt>
		<dl>A movie about a magical ring that many people chase for the power of that ring</dl>
		<dt>The Last Emperor</dt>
		<dl>A brief life of the last Chinese emperor "Puyi"</dl>
		<dt>Avenger</dt>
		<dl>Super hero save the world</dl>
		<dt>Fast and Furious</dt>
		<dl>Few bold guys drive to save the world</dl>
		<dt>Mission impossible</dt>
		<dl>Tom Cruise never aged</dl>
	</dl>	
</p>
	<p>
	<h3>Section 4:Table of dog breeds</h3>
	<table border="1">
		<tr>
			<th>Dog Breeds</th>
			<th>Average Height</th>
			<th>Average Weight</th>
			<th>Example dog greed</th>
		</tr>
		<tr>
		    <td>Retrievers</td>
		    <td>2 ft</td>
		    <td>60 lbs</td>
		    <td>Golden Retrievers</td>
		</tr>
		<tr>
			<td>Bulldogs</td>
			<td>15 inches</td>
			<td>50 lbs</td>
			<td>Spike Bulldog</td>
		</tr>
		<tr>
			<td>German Shorthaired</td>
			<td>24 inches</td>
			<td>60 lbs</td>
			<td>English Pointer</td>
		</tr>
		<tr>
			<td>Boxer</td>
			<td>24 inches</td>
			<td>22.5 lbs</td>
			<td>Swarmer</td>
		</tr>
	</table>
	</p>
	<p>
		<h3>Section 5:State Bird, Flower & Tree</h3>
	<table border = 1>
		<tr>
			<th>State</td>
			<th>State Bird</td>
			<th>State Flower</td>
			<th>State tree</td>
		</tr>
		<tr>
			<td>Ohio</td>
			<td>Cardinal</td>
			<td>Carnation</td>
			<td>Ohio Buckeye</td>
		</tr>
		<tr>
			<td>Michigan</td>
			<td>American robin</td>
			<td>Apple Blossom</td>
			<td>Eastern white pine</td>
		</tr>
		<tr>
			<td>Indianna</td>
			<td>Cardinal</td>
			<td>Peony</td>
			<td>Tulip tree</td>
		</tr>
		<tr>
			<td>California</td>
			<td>California quail</td>
			<td>California poppy</td>
			<td>Coast redwood</td>
		</tr>
		<tr>
			<td>Florida</td>
			<td>Northern mockingbird</td>
			<td>Orange blossom</td>
			<td>Sabal Palm</td>
		</tr>
	</table>
	</p>
	<p>
		<!-- Hexvalue -->
		<h3 style ="color:#FF0000">Section 6:Resturant Menu</h3>
		<table border = "1">
		<tr>
		<!--rgb-->
			<th colspan =5 , style ="color:rgb(255,0,0);">Meal</th>
		</tr>
		<tr>
			<th colspan =2></th>
			<!-- hsla -->
			<th style= "color:hsla(0,100%,50%,1.0)">Breakfast</th>
			<!-- rgba -->
			<th style = "color:rgba(255, 0, 0, 1)">Lunch</th>
			<!-- red -->
			<th style = "color:red">Dinner</th>
		</tr>
		<tr>
			<!--hsl-->
			<th rowspan=4 , style="color:hsl(0,100%,50%);">Food </td>
			<th> Vegetable </th>
			<td> Broccoli </td>
			<td> Cauliflower</td>
			<td> Zuccini</td>
		</tr>
		<tr>
			<th>Meat</th>
			<td>Chicken</td>
			<td>Fish</td>
			<td>Beef</td>
		</tr>
		<tr>
			<th>Dessert</th>
			<td>Chesscake</td>
			<td>Protein Bar</td>
			<td>Ice Cream</td>
		</tr>
		<tr>
			<th>Fruit</th>
			<td>Strawberry</td>
			<td>Apple</td>
			<td>Peach</td>
		</tr>
		</table>
	<a href="#top">Back to top</a>
	</p>
</body>
</html>